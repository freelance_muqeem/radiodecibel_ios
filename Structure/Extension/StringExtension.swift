//
//  StringExtension.swift
//  PaySii
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y);
        var indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        indexOfCharacter = indexOfCharacter + 4
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension UserDefaults {
    
    func object<T: Codable>(_ type: T.Type, with key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = self.value(forKey: key) as? Data else { return nil }
        return try? decoder.decode(type.self, from: data)
    }
    
    func set<T: Codable>(object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder()) {
        let data = try? encoder.encode(object)
        self.set(data, forKey: key)
    }
}

extension Int {
    
    private static var numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        return numberFormatter
    }()
    
    var decimal: String {
        return Int.numberFormatter.string(from: NSNumber(value: self)) ?? ""
    }
}

extension Double {
    
    private static var numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        return numberFormatter
    }()
    
    var decimal: String {
        return Double.numberFormatter.string(from: NSNumber(value: self)) ?? ""
    }
}

extension Array {
    
    func filterDuplicates(_ includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
        
        var results = [Element]()
        
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        
        return results
    }
    
}

extension Data {
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension Double {
    
    static func / (left: Double, right: Int) -> Double {
        return left / Double(right)
    }
    
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
    
}

extension String {
    
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else {
            return nil
        }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    func toInt() -> Int? {
        return NumberFormatter().number(from: self)?.intValue
    }
    
    func isEmptyOrWhitespace() -> Bool {
        if(self.isEmpty) {
            return true
        }
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces) == ""
    }
    
    func removeNumbers() -> String {
        return components(separatedBy: CharacterSet.decimalDigits).joined(separator: "")
    }
    
    func removeLetters() -> String {
        return components(separatedBy: CharacterSet.letters).joined(separator: "")
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func removingLeadingSpaces() -> String {
        guard let index = firstIndex(where: { !CharacterSet(charactersIn: String($0)).isSubset(of: .whitespaces) }) else {
            return self
        }
        return String(self[index...])
    }
    
    func removeStartLetters(length:Int) -> String {
        if length <= 0 {
            return self
        } else if let to = self.index(self.startIndex, offsetBy: length, limitedBy: self.endIndex) {
            return self.substring(from: to)
            
        } else {
            return ""
        }
    }
    
    func GetDate(format: String) -> String {
        
        var serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter2: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter3: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter4: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverDateFormatter5: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd-MM-yyyy HH:mm:ss a"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 5) as TimeZone
            return result
        }()
        
        if let localDate = serverDateFormatter.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        
        
        if let localDate = serverDateFormatter2.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        
        if let localDate = serverDateFormatter3.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        if let localDate = serverDateFormatter4.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
        
        if let localDate = serverDateFormatter5.date(from: self) {
            //            2018-02-04T10:04:00
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
            
            
        else {
            serverDateFormatter = {
                let result = DateFormatter()
                result.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
                return result
            }()
            
            let localDate = serverDateFormatter.date(from: self)!
            
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = format
                return result
            }()
            
            return localDateFormatter.string(from: localDate)
        }
    }
    
    
    
    func GetTime() -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd/mm/yyyy hh:mm:ss a"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(toFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = toFormat
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(fromFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = fromFormat
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        
        return localTimeFormatter.string(from: localTime)
    }
    
    func GetTime(fromFormat: String, toFormat: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = fromFormat
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
            return result
        }()
        
        let serverTime = self
        let localTime = serverDateFormatter.date(from: serverTime)
        
        let localTimeFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = toFormat
            return result
        }()
        
        if localTime != nil {
            return localTimeFormatter.string(from: localTime!)
        }
        else {
            return ""
        }
        
    }
}




