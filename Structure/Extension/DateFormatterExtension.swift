//
//  DateFormatterExtension.swift
//  Radio Decibel
//
//  Created by Junaid Ali on 24/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static var MySQLDateFormat: DateFormatter {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 3600)
        
        return formatter
    }
}
