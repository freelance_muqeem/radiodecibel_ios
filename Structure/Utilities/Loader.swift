//
//  Loader.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 16/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit
import iProgressHUD

let iprogress: iProgressHUD = iProgressHUD()

extension UIViewController {
    
    func showSpinner(view : UIView) {
        
        iprogress.isShowModal = true
        iprogress.isShowCaption = false
        iprogress.isTouchDismiss = false
        iprogress.indicatorStyle = .circleStrokeSpin
        iprogress.indicatorColor = .white
        iprogress.iprogressStyle = .horizontal
        iprogress.indicatorView.startAnimating()
        iprogress.attachProgress(toView: view)
        view.showProgress()
        
    }
    
    func hideSpinner(view : UIView) {
        iprogress.indicatorView.stopAnimating()
        view.dismissProgress()
    }
    
}
