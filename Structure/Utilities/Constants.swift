//
//  Constants.swift
//  PaySii
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit
    
public let THEME_COLOR: UInt = 0x4895CE

public let Main_View_BG_Color = UIColor.init(rgb: 0x1B417C)
public let Splash_View_BG_Color = UIColor.init(rgb: 0x4895CE)

public let BASE_URL = "http://167.114.24.251:96/" // Development

public let LOGO_IMAGE: UIImage = UIImage(named:"logo")!
public let BACK_IMAGE: UIImage = UIImage(named:"back_arrow")!

let USERUPDATED = "UserUpdated"

let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

// Extra

public let User_data_userDefault   = "User_data_userDefault"
public let token_userDefault   = "token_userDefault"
public let NOTIFICATION_USERUPDATE = "user_update"

public let WARNING:UIColor = UIColor.init(named: "Warning_Color")!
public let FAILURE:UIColor = UIColor.init(named: "Failure_Color")!
public let SUCCESS:UIColor = UIColor.init(named: "Success_Color")!

