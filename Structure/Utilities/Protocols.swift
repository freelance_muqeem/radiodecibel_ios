//
// Protocol.swift
// PaySii
//
// Created by mac on 5/14/18.
// Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

protocol RefreshDelegate {
    func StartRefresh()
}

protocol HomeListCellDelegate {
    func playAction(sender: HomeListCell, indexPath:IndexPath)
    func pauseAction(sender: HomeListCell, indexPath:IndexPath)
}

protocol PlayChatCellDelegate {
    func playAction(sender: AudioCell, indexPath:IndexPath)
    func pauseAction(sender: AudioCell, indexPath:IndexPath)
}
