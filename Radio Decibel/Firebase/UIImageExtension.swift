//
//  UIImageExtension.swift
//  Radio Decibel
//
//  Created by Junaid Ali on 09/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage

extension UIImageView {
    
    func setImage(firebaseStoragePath: String) {
        
        let firebaseStorage = Storage.storage()
        let imageRef = firebaseStorage.reference(forURL: firebaseStoragePath)
        
        imageRef.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            
            if let error = error {
                print(error)
            }
            else {
                self.image = UIImage(data: data!)
            }
        }
    }
}
