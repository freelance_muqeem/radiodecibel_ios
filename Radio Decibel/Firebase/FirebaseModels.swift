//
//  RadioStreams.swift
//  Radio Decibel
//
//  Created by Junaid Ali on 09/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

struct RadioStreams: Codable {
    
    let background_color: String
    let logo_path: String
    let radio_id: Int
    let stream_name: String
    let stream_url: String
    let artwork: String
    
}

struct User: Codable {
    
    let fullName: String
    let isBlockedFromChatRoom: Bool
    
}

enum MessageType: String, Codable {
    case TEXT_MESSAGE, IMAGE_MESSAGE, AUDIO_MESSAGE
}

struct Message: Codable {
    
    // general fields
    let messageType: MessageType
    let authorUID: String
    let authorName: String
    let time: Timestamp
    
    // text message fields
    var content: String?
    
    // image message fields
    var imageURL: String?
    
    // audio message fields
    var audioFileURL: String?
    var recordTimeInMillis: Int?
    
    init(authorUID: String, authorName: String, content: String) {
        self.messageType = .TEXT_MESSAGE
        self.authorUID = authorUID
        self.authorName = authorName
        self.content = content
        self.time = Timestamp()
    }
    
    init(authorUID: String, authorName: String, imageURL: String) {
        self.messageType = .IMAGE_MESSAGE
        self.authorUID = authorUID
        self.authorName = authorName
        self.imageURL = imageURL
        self.time = Timestamp()
    }
    
    init(authorUID: String, authorName: String, audioFileURL: String, recordTimeInMillis: Int) {
        self.messageType = .AUDIO_MESSAGE
        self.authorUID = authorUID
        self.authorName = authorName
        self.audioFileURL = audioFileURL
        self.recordTimeInMillis = recordTimeInMillis
        self.time = Timestamp()
    }
    
}

struct PlaylistItem: Codable {
    
    let title_id: Int
    let title_name: String
    let artist_name: String
    let station_name: String
    let updated_at: Date
    
}
