//
//  ImageCell.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 14/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet weak var lblDate:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
