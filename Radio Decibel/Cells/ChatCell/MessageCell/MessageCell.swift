//
//  MessageCell.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 14/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var chatView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        self.chatView.roundCorners(corners: [.topRight, .bottomLeft, .bottomRight], radius: 8)
        self.chatView.layoutIfNeeded()
        
    }
    
}
