//
//  AudioCell.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 15/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class AudioCell: UITableViewCell {

    @IBOutlet weak var audioView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPlayTime:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playButton: UIButton!
    
    var playChatCellDelegate:PlayChatCellDelegate?
    var indexPath:IndexPath?
    var isPlaying:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        loadingIndicator.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        self.audioView.cornerRadius = self.audioView.frame.height * 0.45
        self.audioView.layoutIfNeeded()
        
    }
    
    @IBAction func playButtonAction(_ sender : UIButton) {
        
        if self.isPlaying == true {
            self.playChatCellDelegate?.pauseAction(sender: self, indexPath: indexPath!)
        }
        else {
            self.playChatCellDelegate?.playAction(sender: self, indexPath: indexPath!)
        }
        
        
    }
 
    func showLoader() {
        playButton.isHidden = true
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    func showPlayButton() {
        isPlaying = false
        
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true

        self.playButton.setImage(UIImage(named: "play"), for: .normal)
        self.playButton.isHidden = false
    }
    
    func showStopButton() {
        isPlaying = true
        
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true
        
        self.playButton.setImage(UIImage(named: "pause"), for: .normal)
        self.playButton.isHidden = false
    }
    
}
