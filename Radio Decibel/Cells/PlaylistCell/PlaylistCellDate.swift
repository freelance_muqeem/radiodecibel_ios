//
//  PlaylistCell.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 23/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class PlaylistCellDate: UITableViewCell {

    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var lblSongName:UILabel!
    @IBOutlet weak var lblArtistName:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
