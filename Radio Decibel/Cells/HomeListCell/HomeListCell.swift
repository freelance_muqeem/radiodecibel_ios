//
//  HomeListCell.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 07/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeListCell: UITableViewCell {

    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var imgPlayButton:UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var homeListCellDelegate:HomeListCellDelegate?
    var indexPath:IndexPath?
    var isPlaying:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        loadingIndicator.isHidden = true
    }
    
    @IBAction func playAction(_ sender:UIButton) {
        
        if self.isPlaying == true {
            self.homeListCellDelegate?.pauseAction(sender: self, indexPath: indexPath!)
        }
        else {
            self.homeListCellDelegate?.playAction(sender: self, indexPath: indexPath!)
        }
        
    }
    
    func showLoader() {
        isPlaying = false
        
        self.imgPlayButton.isHidden = true
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
    }
    
    func showPlayButton() {
        isPlaying = false
        
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true

        self.imgPlayButton.image = UIImage(named: "play")
        self.imgPlayButton.isHidden = false
    }
    
    func showStopButton() {
        isPlaying = true
        
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true
        
        self.imgPlayButton.image = UIImage(named: "pause")
        self.imgPlayButton.isHidden = false
    }
    
}
