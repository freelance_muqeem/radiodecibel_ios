//
//  PlaylistViewController.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 23/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import OHMySQL

extension PlaylistViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if (textField.text?.isEmpty)! || textField.text == "" {
            
            self.search = false
            self.btnCancel.isHidden = true
            
            self.txtSearch.becomeFirstResponder()
            return
            
        }
        
        self.search = true
        self.btnCancel.isHidden = false
        // self.tblView.reloadData()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtSearch {
        
            if !(textField.text?.isEmpty)! && textField.text != "" {
            
                self.resetDaysFilterViews()
                
                self.getPlaylistBySearch(keyword: (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)
            }
            else {
                self.showBanner(title: "Warning", subTitle: "Enter some keyword to search", type: WARNING)
            }
            
            textField.resignFirstResponder()
        }
        return true
    }
    
}

class PlaylistViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> PlaylistViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PlaylistViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    @IBOutlet weak var SearchView:UIView!
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var btnCancel:UIButton!
    
    @IBOutlet weak var lblToday:UILabel!
    @IBOutlet weak var lblYesterday:UILabel!
    @IBOutlet weak var lblDayBeforeYesterday:UILabel!
    
    @IBOutlet weak var todayView:UIView!
    @IBOutlet weak var yesterdayView:UIView!
    @IBOutlet weak var daybeforeView:UIView!
    
    var pullToRefreshControl = UIRefreshControl()
    
    var search:Bool = false
    
    // MySQL database variables
    var databaseUser: OHMySQLUser? = nil
    var databaseCoordinator: OHMySQLStoreCoordinator? = nil
    var databaseQueryContext = OHMySQLQueryContext()
    let decoder = JSONDecoder()
    
    // array to store all the displaying playlist items
    var playlistItems: [PlaylistItem] = []
    
    // date formatter to show time on playlist item
    var playlistItemDateFormatter = DateFormatter()
    
    // date formatter for database query
    var databaseQueryDateFormatter = DateFormatter()
    
    // dictionary to store date sections and list of playlist items
    var sections = Dictionary<Date, Array<PlaylistItem>>()
    var sortedSectionsKeys = [Date]()
    
    // date formatter for date sections in tableview
    var sectionDateFormatter = DateFormatter()
    
    // available options to filter playlist
    enum FilterOption {
        case search
        case today
        case yesterday
        case dayBeforeYesterday
        case date
    }
    
    // currently selected filter option, by default today is selected
    var selectedFilterOption = FilterOption.today
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register Cell
        let cell = UINib(nibName: "PlaylistCell", bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: "PlaylistCell")
        
        let dateCell = UINib(nibName: "PlaylistCellDate", bundle: nil)
        self.tblView.register(dateCell, forCellReuseIdentifier: "PlaylistCellDate")
        
        self.setupView()
        
        // set json decoder date decoding strategy
        decoder.dateDecodingStrategy = .formatted(DateFormatter.MySQLDateFormat)
        
        // set playlist item date format
        playlistItemDateFormatter.timeZone = .autoupdatingCurrent
        playlistItemDateFormatter.dateFormat = "HH:mm"
        
        // set database query date format
        databaseQueryDateFormatter.timeZone = TimeZone(secondsFromGMT: 3600)
        databaseQueryDateFormatter.dateFormat = "yyyy-MM-dd"
        
        // set tableview section date format
        sectionDateFormatter.timeZone = .autoupdatingCurrent
        sectionDateFormatter.dateFormat = "dd MMMM, yyyy"
        
        // make connection to database
        self.connectToDatabase()
        
        // fetch today playlist by default
        self.getTodayPlaylist()
                
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        // disconnect from database
        self.disconnectToDatabase()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.SearchView.cornerRadius = self.SearchView.frame.height * 0.45
        self.view.layoutIfNeeded()
        
    }
    
    
    func setupView() {
        
        self.title = "Playlist"
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        
        self.search = false
        self.btnCancel.isHidden = true
        
        self.tblView.refreshControl = pullToRefreshControl
        pullToRefreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        self.yesterdayView.isHidden = true
        self.daybeforeView.isHidden = true
        
        self.lblYesterday.textColor = UIColor.white
        self.lblDayBeforeYesterday.textColor = UIColor.white
        
        self.txtSearch.returnKeyType = .search
        self.txtSearch.delegate = self
        self.txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
    }
    
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func todayAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        // clear search text
        self.txtSearch.text = ""
        // un select all the days filter
        self.resetDaysFilterViews()
        // select today filter
        self.todayView.isHidden = false
        self.lblToday.textColor = UIColor.init(rgb: THEME_COLOR)
        
        
        // get today playlist data
        self.getTodayPlaylist()
    }
    
    @IBAction func yesterdayAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        // clear search text
        self.txtSearch.text = ""
        // un select all the days filter
        self.resetDaysFilterViews()
        // select yesterday filter
        self.yesterdayView.isHidden = false
        self.lblYesterday.textColor = UIColor.init(rgb: THEME_COLOR)
        
        
        // get yesterday playlist data
        self.getYesterdayPlaylist()
    }
    
    @IBAction func dayBeforeAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        // clear search text
        self.txtSearch.text = ""
        // un select all the days filter
        self.resetDaysFilterViews()
        // select day before yesterday filter
        self.daybeforeView.isHidden = false
        self.lblDayBeforeYesterday.textColor = UIColor.init(rgb: THEME_COLOR)
        
        
        // get day before yesterday playlist data
        self.getDayBeforeYesterdayPlaylist()
    }
    
    
    @IBAction func searchCancelAction(_ sender : UIButton) {
        self.txtSearch.text = ""
        
        self.view.endEditing(true)
        
        // un select all the days filter
        self.resetDaysFilterViews()
        // select today filter
        self.todayView.isHidden = false
        self.lblToday.textColor = UIColor.init(rgb: THEME_COLOR)
        
        
        // get today playlist data
        self.getTodayPlaylist()
    }
    
    @IBAction func calendarAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Select Date", datePickerMode: .date, selectedDate: Date(), minimumDate: nil, maximumDate: Date(), doneBlock: { (picker, date, origin) in
            
            self.selectedFilterOption = .date
            
            // clear search text
            self.txtSearch.text = ""
            // un select all the days filter
            self.resetDaysFilterViews()
            
            self.getPlaylistOfDate(date: self.databaseQueryDateFormatter.string(from: date as! Date))
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    func resetDaysFilterViews() {
        self.todayView.isHidden = true
        self.yesterdayView.isHidden = true
        self.daybeforeView.isHidden = true
        
        self.lblToday.textColor = UIColor.white
        self.lblYesterday.textColor = UIColor.white
        self.lblDayBeforeYesterday.textColor = UIColor.white
    }
    
    @objc func refresh(_ sender: Any) {
        
        switch selectedFilterOption {
        
        case .today:
            self.getTodayPlaylist()
        case .yesterday:
            self.getYesterdayPlaylist()
        case .dayBeforeYesterday:
            self.getDayBeforeYesterdayPlaylist()
        case .search:
            
            if !(self.txtSearch.text?.isEmpty)! && txtSearch.text != "" {
            
                self.resetDaysFilterViews()
                
                self.getPlaylistBySearch(keyword: (txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)
            }
            else {
                self.showBanner(title: "Warning", subTitle: "Enter some keyword to search", type: WARNING)
            }
            
        case .date:
            // no need to update in this case
            pullToRefreshControl.endRefreshing()
        }
        
        pullToRefreshControl.endRefreshing()
    }
    
}

//MARK: TableView Delegate & DataSource
extension PlaylistViewController : UITableViewDelegate , UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.isEmpty ? 1 : sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.isEmpty ? playlistItems.count : sections[sortedSectionsKeys[section]]!.count
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let playlistItem = sections.isEmpty ? playlistItems[indexPath.row] : sections[sortedSectionsKeys[indexPath.section]]![indexPath.row]
        
        let playlistCell: PlaylistCell = tableView.dequeueReusableCell(withIdentifier: "PlaylistCell", for: indexPath) as! PlaylistCell
        
        playlistCell.lblTime.text = playlistItemDateFormatter.string(from: playlistItem.updated_at)
        playlistCell.lblSongName.text = playlistItem.title_name
        playlistCell.lblArtistName.text = playlistItem.artist_name
        
        return playlistCell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections.isEmpty ? "" : sectionDateFormatter.string(from: sortedSectionsKeys[section])
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        view.tintColor = UIColor(hex: "#025c9e")
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
    }
    
}


// MARK: MYSQL database read playlist data

extension PlaylistViewController {
    
    func connectToDatabase() {
        
        self.databaseUser = OHMySQLUser(userName: "readonlydb", password: "g*J28bk2", serverName: "webhosting.globallogic.nl", dbName: "decibelplayer_", port: 3306, socket: "var/lib/mysql/mysql.sock")
        
        self.databaseCoordinator = OHMySQLStoreCoordinator(user: databaseUser!)
        self.databaseCoordinator?.encoding = .UTF8MB4
        
        databaseCoordinator!.connect()
        
        databaseQueryContext.storeCoordinator = databaseCoordinator!
        
    }
    
    func disconnectToDatabase() {
        
        databaseCoordinator?.disconnect()
    }
    
    func getPlaylistOfDate(date: String) {
        
        // clear sections and relative playlist items
        sections.removeAll()
        sortedSectionsKeys.removeAll()
        
        self.showSpinner(view: self.view)
        
        let query = OHMySQLQueryRequest(queryString: "select distinct updated_at, title_id, title_name, artist_name, station_name from decibelplayer_.radio_log where station_name = 'DB' AND mix_duration >= '170000' AND artist_name <> '' AND title_name <> '' AND date(updated_at) = '\(date)' order by id desc;")
        
        if ((databaseCoordinator?.isConnected) != nil) {
            
            DispatchQueue.global(qos: .userInitiated).async {
                do {
                    
                    let response = try self.databaseQueryContext.executeQueryRequestAndFetchResult(query)
        
                    let responseInJson = try JSONSerialization.data(withJSONObject: response)
                    let decodedPlaylistItems = try self.decoder.decode([PlaylistItem].self, from: responseInJson)
                    
                    DispatchQueue.main.async {
        
                        self.playlistItems.removeAll()
                        self.playlistItems.append(contentsOf: decodedPlaylistItems)
                        
                        self.tblView.reloadData()
                        
                        self.hideSpinner(view: self.view)
                    }
                } catch let error {
                    
                    DispatchQueue.main.async {
                        print(error)
                        self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
                    }
                }
            }
        }
        else{
            self.showBanner(title: "Error", subTitle: "Failed to get the playlist", type: FAILURE)
        }
    }
    
    func getTodayPlaylist() {
        
        self.selectedFilterOption = FilterOption.today
        
        self.getPlaylistOfDate(date:self.databaseQueryDateFormatter.string(from: Date()))
        
    }
    
    func getYesterdayPlaylist() {
        
        self.selectedFilterOption = FilterOption.yesterday
        
        self.getPlaylistOfDate(date: self.databaseQueryDateFormatter.string(from: Date().incrementDays(numberOfDays: -1)))
    }
    
    func getDayBeforeYesterdayPlaylist() {
        
        self.selectedFilterOption = FilterOption.dayBeforeYesterday
        
        self.getPlaylistOfDate(date: self.databaseQueryDateFormatter.string(from: Date().incrementDays(numberOfDays: -2)))
    }
    
    func getPlaylistBySearch(keyword: String) {
        
        self.selectedFilterOption = FilterOption.search
        
        // clear playlist items
        playlistItems.removeAll()
        
        self.showSpinner(view: self.view)
        
        let query = OHMySQLQueryRequest(queryString: "select distinct updated_at, title_id, title_name, artist_name, station_name from decibelplayer_.radio_log where station_name = 'DB' AND mix_duration >= '170000' AND artist_name like '%\(keyword)%' OR title_name like '%\(keyword)%' order by id desc;")
        
        if ((databaseCoordinator?.isConnected) != nil) {
            
            DispatchQueue.global(qos: .userInitiated).async {
                do {
                    
                    let response = try self.databaseQueryContext.executeQueryRequestAndFetchResult(query)
        
                    let responseInJson = try JSONSerialization.data(withJSONObject: response)
                    let decodedPlaylistItems = try self.decoder.decode([PlaylistItem].self, from: responseInJson)
                    
                    self.sections.removeAll()
                    self.sortedSectionsKeys.removeAll()
                    
                    self.sections = self.createDateSections(playlistItems: decodedPlaylistItems)
                    self.sortedSectionsKeys = self.sections.keys.sorted(by: { $0.compare($1) == .orderedDescending })
                    
                    DispatchQueue.main.async {
        
                        self.tblView.reloadData()
                        
                        self.hideSpinner(view: self.view)
                    }
                } catch let error {
                    
                    DispatchQueue.main.async {
                        print(error)
                        self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
                    }
                }
            }
        }
        else{
            self.showBanner(title: "Error", subTitle: "Failed to get the playlist", type: FAILURE)
        }
    }
    
    func createDateSections(playlistItems: [PlaylistItem]) -> Dictionary<Date, Array<PlaylistItem>> {
        
        var tableViewSections = Dictionary<Date, Array<PlaylistItem>>()
        
        for playlistItem in playlistItems {
            
            let date = removeTimeStamp(fromDate: playlistItem.updated_at)
            
            if tableViewSections.index(forKey: date) == nil {
                tableViewSections[date] = [playlistItem]
            }
            else {
                tableViewSections[date]!.append(playlistItem)
            }
        }
        
        return tableViewSections
    }
    
    func removeTimeStamp(fromDate: Date) -> Date {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: fromDate)) else {
            fatalError("Failed to strip time from Date object")
        }
        return date
    }
}
