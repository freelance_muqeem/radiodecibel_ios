//
//  ChatScreenViewController.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 13/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit
import ALCameraViewController
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import SDWebImage
import AVFoundation

extension ChatScreenViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if (textField.text?.isEmpty)! || textField.text == "" || textField.text!.isEmptyOrWhitespace() {
            self.btnSend.isHidden = true
            self.tblView.reloadData()
            self.scrollToBottom()
            return
        }
        
        self.btnSend.isHidden = false
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.scrollToBottom()
            
            UIView.animate(withDuration: 0.20, animations: {
                self.view.layoutIfNeeded()
                
                self.chatViewBottomConstraint.constant = keyboardSize.height + 20
            })
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.20, animations: {
            self.view.layoutIfNeeded()
            
            self.chatViewBottomConstraint.constant = 20
        })
        
        self.scrollToBottom()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if self.txtMessage.text!.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please write something to send", type: FAILURE)
            return true
        }
        
        if let messageText = txtMessage.text {
            
            txtMessage.text = ""
            
            //send message to server
            self.sendMessage(messageContent: messageText)
        }
        
        return true
    }
    
}

class ChatScreenViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ChatScreenViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ChatScreenViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var chatView:UIView!
    
    @IBOutlet weak var txtMessage:UITextField!
    @IBOutlet weak var btnSend:UIButton!
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var audioBoxView:UIView!
    @IBOutlet weak var micView:UIView!
    @IBOutlet weak var lblAudioMessageTime:UILabel!
    
    @IBOutlet weak var chatViewBottomConstraint: NSLayoutConstraint!
    
    // Audio Message variables
    var audioRecordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var recordingTimer: Timer?
    var milliseconds: Int = 0
    
    // Audio Player variables
    var audioMessagesPlayer: AVAudioPlayer?
    // last selected cell index path
    var selectedAudioMessageIndexPath: IndexPath?
    
    
    var messagesListener: ListenerRegistration? = nil
    // array to store all the fetched messages
    var chatRoomMessages: [Message] = []
    // current logged in user
    var currentUser: User? = nil
    // date formatter for displaying date on messages
    var dateFormatter = DateFormatter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register Cell
        let messageCell = UINib(nibName: "MessageCell", bundle: nil)
        self.tblView.register(messageCell, forCellReuseIdentifier: "MessageCell")
        
        let imageCell = UINib(nibName: "ImageCell", bundle: nil)
        self.tblView.register(imageCell, forCellReuseIdentifier: "ImageCell")
        
        let audioCell = UINib(nibName: "AudioCell", bundle: nil)
        self.tblView.register(audioCell, forCellReuseIdentifier: "AudioCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.setupView()
        self.getCurrentUser() // get current logged in user
        self.getMessagesInRealTime() //attach listener to get messages in real-time
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        // stop real-time messages
        messagesListener!.remove()
        self.stopAudioMessage()
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            
            if self.chatRoomMessages.count > 0 {
                let indexPath = IndexPath(row: self.chatRoomMessages.count - 1, section: 0)
                self.tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    func setupView() {
        
        self.title = "Chat"
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        
        self.txtMessage.returnKeyType = .send
        self.txtMessage.delegate = self
        self.txtMessage.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        self.shadowView.isHidden = true
        self.audioBoxView.isHidden = true
        self.micView.isHidden = true
        self.btnSend.isHidden = true
        
        self.chatView.cornerRadius = self.chatView.frame.height * 0.45
        self.view.layoutIfNeeded()
        
        self.dateFormatter.dateFormat = "dd MMM yyyy, h:mm a"
    }
    
    @objc func backAction() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
            else {
                let vc = HomeViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func cameraAction(_ sender : UIButton) {
        self.openCamera()
    }
    
    @IBAction func micAction(_ sender : UIButton) {
        
        self.getRecordingAudioPermission()
    }
    
}

//MARK: TableView Delegate & DataSource
extension ChatScreenViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatRoomMessages.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let message = self.chatRoomMessages[indexPath.row]
        
        if message.messageType == .TEXT_MESSAGE {
            return UITableView.automaticDimension
        }
        
        if message.messageType == .AUDIO_MESSAGE {
            return 100.0
        }
        
        return 250.0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let message = self.chatRoomMessages[indexPath.row]
        
        if message.messageType == .TEXT_MESSAGE {
            return UITableView.automaticDimension
        }
        
        if message.messageType == .AUDIO_MESSAGE {
            return 100.0
        }
        
        return 250.0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = self.chatRoomMessages[indexPath.row]
        
        if message.messageType == .TEXT_MESSAGE {
            
            let messageCell: MessageCell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
            
            messageCell.lblName.text = message.authorName
            messageCell.lblMessage.text = message.content
            messageCell.lblDate.text = dateFormatter.string(from: message.time.dateValue())
            
            return messageCell
            
        }
        else if message.messageType == .IMAGE_MESSAGE {
            
            let imageCell: ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell") as! ImageCell
            
            imageCell.lblName.text = message.authorName
            imageCell.imgView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            imageCell.imgView.sd_setImage(with: Storage.storage().reference(forURL: message.imageURL ?? ""))
            imageCell.lblDate.text = dateFormatter.string(from: message.time.dateValue())
            
            return imageCell
            
        }
        else if message.messageType == .AUDIO_MESSAGE {
            
            let audioCell: AudioCell = tableView.dequeueReusableCell(withIdentifier: "AudioCell") as! AudioCell
            
            audioCell.lblName.text = message.authorName
            audioCell.lblDate.text = dateFormatter.string(from: message.time.dateValue())
            audioCell.lblPlayTime.text = getTimeInString(milliseconds: message.recordTimeInMillis ?? 0)
            
            audioCell.playChatCellDelegate = self
            audioCell.indexPath = indexPath
            
            if self.selectedAudioMessageIndexPath != nil {
                if self.selectedAudioMessageIndexPath?.row != indexPath.row {
                    audioCell.showPlayButton()
                }
            }
            
            return audioCell
        }
        
        return UITableViewCell()
    }
    
}

//MARK:- Text Message Functionality
extension ChatScreenViewController {
    
    @IBAction func sendMessageAction(_ sender:UIButton) {
        
        if self.txtMessage.text!.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please write something to send", type: FAILURE)
            return
        }
        
        if let messageText = txtMessage.text {
            
            txtMessage.text = ""
            
            //send message to server
            self.sendMessage(messageContent: messageText)
        }
    }
}

//MARK:- Audio Message Functionality
extension ChatScreenViewController : PlayChatCellDelegate, AVAudioRecorderDelegate {
    
    @IBAction func cancelAudioMessageAction(_ sender:UIButton) {
        
        cancelRecording()
    }
    
    @IBAction func sendAudioMessageAction(_ sender:UIButton) {
        
        saveRecording()
    }
    
    func playAction(sender: AudioCell, indexPath: IndexPath) {
        
        self.getDownloadURLOfAudioMessage(firebaseStoragePath: self.chatRoomMessages[indexPath.row].audioFileURL!, indexPath: indexPath)
        
    }
    
    func pauseAction(sender: AudioCell, indexPath: IndexPath) {
        
        self.stopAudioMessage()
    }
    
    func getRecordingAudioPermission() {
        
        audioRecordingSession = AVAudioSession.sharedInstance()
        
        do {
            try audioRecordingSession.setCategory(.playAndRecord, mode: .default)
            try audioRecordingSession.setActive(true)
            audioRecordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.showAudioMessageDialog()
                        self.startRecording()
                    } else {
                        // failed to record!
                        self.showBanner(title: "Error", subTitle: "Can't record audio message without permission", type: FAILURE)
                    }
                }
            }
        } catch let error {
            print(error.localizedDescription)
            self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
        }
    }
    
    func showAudioMessageDialog() {
        self.view.endEditing(true)
        self.shadowView.isHidden = false
        self.view.setView(view: self.audioBoxView, hidden: false)
        self.audioBoxView.setView(view: self.micView, hidden: false)
        self.view.layoutIfNeeded()
        
        self.lblAudioMessageTime.text = "00:00"
    }
    
    func hideAudioMessageDialog() {
        self.view.endEditing(true)
        self.shadowView.isHidden = true
        self.view.setView(view: self.audioBoxView, hidden: true)
        self.audioBoxView.setView(view: self.micView, hidden: true)
        self.view.layoutIfNeeded()
    }
    
    @objc func updateTimeLabel(timer: Timer) {
        milliseconds += 1
        
        self.lblAudioMessageTime.text = getTimeInString(milliseconds: milliseconds)
    }
    
    func getTimeInString(milliseconds: Int) -> String {
        let sec = (milliseconds / 60) % 60
        let min = (milliseconds / 3600)
        
        return String(format: "%02d:%02d", min,sec) as String
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func startRecording() {
        
        recordingTimer?.invalidate()
        milliseconds = 0
        
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            recordingTimer = Timer.scheduledTimer(timeInterval: 0.0167, target: self, selector: #selector(updateTimeLabel(timer:)), userInfo: nil, repeats: true)
            audioRecorder.record()
            
        } catch {
            hideAudioMessageDialog()
            self.showBanner(title: "Error", subTitle: "Failed to record message", type: FAILURE)
        }
    }
    
    func saveRecording() {
        
        recordingTimer?.invalidate()
        
        hideAudioMessageDialog()
        
        self.audioRecorder.stop()
        
        self.uploadAudio(fileURL: self.audioRecorder.url,recordTimeInMillis: milliseconds)
    }
    
    func cancelRecording() {
        
        recordingTimer?.invalidate()
        
        hideAudioMessageDialog()
        
        self.audioRecorder.deleteRecording()
        self.audioRecorder = nil
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            // audio recording cancelled by system because of some reason like Call etc.
            cancelRecording()
        }
    }
}


// MARK: Audio Message Player
extension ChatScreenViewController: AVAudioPlayerDelegate {
    
    func playAudioMessage(fileURL: URL, indexPath:IndexPath) {
        
        if let path = selectedAudioMessageIndexPath {
            
            if let player  = audioMessagesPlayer {
                
                if player.isPlaying {
                    
                    audioMessagesPlayer?.stop()
                    selectedAudioMessageIndexPath = nil
                    
                    DispatchQueue.main.async {
                        let cell: AudioCell = self.tblView.cellForRow(at: path) as! AudioCell
                        cell.showPlayButton()
                    }
                }
            }
        }
        
        do {
            
            audioMessagesPlayer = try AVAudioPlayer(contentsOf: fileURL)
            audioMessagesPlayer?.delegate = self
            audioMessagesPlayer?.play()
            
            selectedAudioMessageIndexPath = indexPath
            DispatchQueue.main.async {
                let cell: AudioCell = self.tblView.cellForRow(at: self.selectedAudioMessageIndexPath!) as! AudioCell
                cell.showStopButton()
            }
            
        } catch let error {
            print(error.localizedDescription)
            self.showBanner(title: "Error", subTitle: "Failed to play audio message", type: FAILURE)
        }
    }
    
    func stopAudioMessage() {
        
        if let indexPath = selectedAudioMessageIndexPath {
            
            if let player  = audioMessagesPlayer {
                
                if player.isPlaying {
                    audioMessagesPlayer?.stop()
                    
                    DispatchQueue.main.async {
                        let cell: AudioCell = self.tblView.cellForRow(at: indexPath) as! AudioCell
                        cell.showPlayButton()
                    }
                }
            }
        }
        
        selectedAudioMessageIndexPath = nil
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if selectedAudioMessageIndexPath != nil {
            DispatchQueue.main.async {
                let cell: AudioCell = self.tblView.cellForRow(at: self.selectedAudioMessageIndexPath!) as! AudioCell
                cell.showPlayButton()
            }
        }
    }
    
}

//MARK:- Camera Functionality
extension ChatScreenViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    func openCamera(){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            
            if let pickedImage = image {
                
                //Image to data
                if let compressedImage = pickedImage.jpegData(compressionQuality: 0.5) {
                    self?.uploadImage(imageData: compressedImage)
                }
            }
            
            self?.dismiss(animated: true, completion: nil)
            
        }
        
        present(cameraViewController, animated: true, completion: nil)
        
    }
    
}


// Write and Read Messages from firestore in realtime
extension ChatScreenViewController {
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
        self.scrollToBottom()
    }
    
    func getMessagesInRealTime() {
        
        let db = Firestore.firestore()
        
        messagesListener = db.collection("messages").order(by: "time").addSnapshotListener { querySnapshot, error in
            
            if let err = error {
                self.showBanner(title: "Error", subTitle: err.localizedDescription, type: FAILURE)
                return
            }
            
            guard querySnapshot != nil else {
                self.showBanner(title: "Error", subTitle: "Failed to get the messages", type: FAILURE)
                return
            }
            
            guard let documents = querySnapshot?.documentChanges else {
                self.showBanner(title: "Alert", subTitle: "No messages found", type: WARNING)
                return
            }
            
            for document in documents {
                
                do {
                    if let message = try document.document.data(as: Message.self) {
                        self.chatRoomMessages.append(message)
                    }
                }
                catch let error {
                    print(error.localizedDescription)
                }
            }
            
            // reload table view to update it
            self.reloadTableView()
        }
        
    }
    
    
    func sendMessage(messageContent: String) {
        
        let db = Firestore.firestore()
        
        guard let currentUserUID = Auth.auth().currentUser?.uid else {
            self.showBanner(title: "Error", subTitle: "User is not logged in", type: FAILURE)
            return
        }
        
        let messageToSend = Message(authorUID: currentUserUID, authorName: self.currentUser!.fullName, content: messageContent)
        
        do {
            try db.collection("messages").addDocument(from: messageToSend)
        }
        catch let error {
            self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
        }
        
    }
    
    func sendImageMessage(imageURL: String) {
        
        let db = Firestore.firestore()
        
        guard let currentUserUID = Auth.auth().currentUser?.uid else {
            self.showBanner(title: "Error", subTitle: "User is not logged in", type: FAILURE)
            return
        }
        
        let messageToSend = Message(authorUID: currentUserUID, authorName: self.currentUser!.fullName, imageURL: imageURL)
        
        do {
            try db.collection("messages").addDocument(from: messageToSend)
        }
        catch let error {
            self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
        }
        
    }
    
    func sendAudioMessage(audioFileURL: String, recordTimeInMillis: Int) {
        
        let db = Firestore.firestore()
        
        guard let currentUserUID = Auth.auth().currentUser?.uid else {
            self.showBanner(title: "Error", subTitle: "User is not logged in", type: FAILURE)
            return
        }
        
        let messageToSend = Message(authorUID: currentUserUID, authorName: self.currentUser!.fullName, audioFileURL: audioFileURL, recordTimeInMillis: recordTimeInMillis)
        
        do {
            try db.collection("messages").addDocument(from: messageToSend)
        }
        catch let error {
            self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
        }
    }
    
    func getCurrentUser() {
        
        if let currentUser = Auth.auth().currentUser {
            
            // user is logged in, check for unblock
            let db = Firestore.firestore()
            
            db.collection("users").document(currentUser.uid).getDocument { (document,error) in
                
                do {
                    if let user = try document?.data(as: User.self) {
                        
                        self.currentUser = user
                        
                    }
                    else {
                        self.showBanner(title: "Error", subTitle: "Failed to get user authentication status", type: FAILURE)
                    }
                }
                catch _ {
                    self.showBanner(title: "Error", subTitle: "Failed to get user authentication status", type: FAILURE)
                }
                
                // hide loader
                self.hideSpinner(view: self.view)
            }
            
        }
    }
    
    func uploadImage(imageData: Data) {
        
        self.showSpinner(view: self.view)
        
        let imageName = UUID().uuidString
        
        let imageStorageRef = Storage.storage().reference().child("messages_images/\(imageName).jpg")
        
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        
        let uploadTask = imageStorageRef.putData(imageData, metadata: metaData)
        
        
        uploadTask.observe(.success) { snapshot in
            self.sendImageMessage(imageURL: "gs://\(snapshot.reference.bucket)/\(snapshot.reference.fullPath)")
            self.hideSpinner(view: self.view)
        }
        
        uploadTask.observe(.failure) { snapshot in
            
            if let error = snapshot.error as NSError? {
                self.hideSpinner(view: self.view)
                self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
            }
        }
        
    }
    
    func uploadAudio(fileURL: URL, recordTimeInMillis: Int) {
        
        self.showSpinner(view: self.view)
        
        let audioFileName = UUID().uuidString
        
        let audiosStorageRef = Storage.storage().reference().child("messages_audios/\(audioFileName).m4a")
        
        let metaData = StorageMetadata()
        metaData.contentType = "audio/m4a"
        
        let uploadTask = audiosStorageRef.putFile(from: fileURL, metadata: metaData)
        
        
        uploadTask.observe(.success) { snapshot in
            self.sendAudioMessage(audioFileURL: "gs://\(snapshot.reference.bucket)/\(snapshot.reference.fullPath)", recordTimeInMillis: recordTimeInMillis)
            self.hideSpinner(view: self.view)
        }
        
        uploadTask.observe(.failure) { snapshot in
            
            if let error = snapshot.error as NSError? {
                self.hideSpinner(view: self.view)
                self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
            }
        }
        
    }
    
    func getDownloadURLOfAudioMessage(firebaseStoragePath: String, indexPath: IndexPath) {
        
        let cell: AudioCell = self.tblView.cellForRow(at: indexPath) as! AudioCell
        cell.showLoader()
        
        let firebaseStorage = Storage.storage()
        let audioRef = firebaseStorage.reference(forURL: firebaseStoragePath)
        
        let localFileURL = self.getDocumentsDirectory().appendingPathComponent("downloadedAudioMessage.m4a")
        
        audioRef.write(toFile: localFileURL) { url, error in
            
            if let error = error {
                self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
                
                DispatchQueue.main.async {
                    cell.showPlayButton()
                }
            }
            else {
                self.playAudioMessage(fileURL: url!, indexPath: indexPath)
            }
            
        }
    }
    
}
