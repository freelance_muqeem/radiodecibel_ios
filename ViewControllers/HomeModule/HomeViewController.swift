//
// HomeViewController.swift
// Radio Decibel
//
// Created by Abdul Muqeem on 07/10/2020.
// Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit
import SwiftAudio
import FirebaseFirestore
import FirebaseStorage
import FirebaseUI
import FirebaseAuth
import SDWebImage
import MediaPlayer

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var btnChat:UIButton!
    @IBOutlet weak var btnPlaylist:UIButton!
    
    // get Firebase cloud storage reference
    let firebaseStorage = Storage.storage()
    
    // array to store all the fetched radio stream objects
    var radioStreams: [RadioStreams] = []
    
    // Audio player object to play stream
    let audioPlayer = AudioPlayer()
    
    // last selected cell index path
    var selectedIndexPath: IndexPath?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register Cell
        let cell = UINib(nibName: "HomeListCell", bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: "HomeListCell")
        
        self.setupAudioPlayer()
        self.getRadioStreams()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupView()
    }
    
    func setupView() {
        self.btnChat.cornerRadius = self.btnChat.frame.height * 0.45
        self.btnPlaylist.cornerRadius = self.btnChat.frame.height * 0.45
        self.tblView.contentInsetAdjustmentBehavior = .never
        self.navigationController?.HideNavigationBar()
        self.LightStatusBar()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func chatAction(_ sender : UIButton) {
        self.isUserLoggedInAndUnblock()
    }
    
    @IBAction func playlistAction(_ sender : UIButton) {
        let vc = PlaylistViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK: Audio player
extension HomeViewController {
    
    func setupAudioPlayer() {
        
        audioPlayer.automaticallyWaitsToMinimizeStalling = true
        
        audioPlayer.automaticallyUpdateNowPlayingInfo = true
        
        audioPlayer.nowPlayingInfoController.set(keyValue: NowPlayingInfoProperty.isLiveStream(true))
        
        audioPlayer.nowPlayingInfoController.set(keyValue: NowPlayingInfoProperty.playbackRate(1.0))
        
        audioPlayer.remoteCommands = [.play, .stop , .pause, .togglePlayPause]
        
        audioPlayer.event.stateChange.addListener(self, handleAudioPlayerStateChange(state:))
        
        audioPlayer.remoteCommandController.handlePlayCommand = self.handlePlayCommand
        
    }
    
    private func handlePlayCommand(event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
        if self.selectedIndexPath != nil {
            
            playStream(stream: self.radioStreams[self.selectedIndexPath!.row])
            return MPRemoteCommandHandlerStatus.success
        }
        return MPRemoteCommandHandlerStatus.commandFailed
    }
    
    func handleAudioPlayerStateChange(state: AudioPlayerState) {
        
        switch state {
        
        case .buffering:
            print("Buffering...")
            
            DispatchQueue.main.async {
                let cell: HomeListCell = self.tblView.cellForRow(at: self.selectedIndexPath!) as! HomeListCell
                cell.showLoader()
            }
            
        case .loading:
            print("Loading stream")
            
        case .ready:
            print("Loaded stream")
            
            try? AudioSessionController.shared.set(category: .playback)
            
            // stream is ready to play, activate audio session
            try? AudioSessionController.shared.activateSession()
            
            // disable intrruption notifications
            AudioSessionController.shared.isObservingForInterruptions = false
            
        case .paused:
            print("Stream Paused")
            
            DispatchQueue.main.async {
                let cell: HomeListCell = self.tblView.cellForRow(at: self.selectedIndexPath!) as! HomeListCell
                cell.showPlayButton()
            }
            
        case .playing:
            print("Stream Playing")
            
            DispatchQueue.main.async {
                let cell: HomeListCell = self.tblView.cellForRow(at: self.selectedIndexPath!) as! HomeListCell
                cell.showStopButton()
            }
            
        case .idle:
            print("Player is idle")
            
        }
        
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    func playStream(stream: RadioStreams) {
        
        // create audio item with stream URL
        let audioItem = DefaultAudioItem(audioUrl: stream.stream_url, artist: "Online Stream", title: stream.stream_name, albumTitle: "", sourceType: .stream, artwork: UIImage())
        
        // try to play the stream
        try? audioPlayer.load(item: audioItem,playWhenReady: true)
        
        // get artwork of the stream
        let artworkRef = self.firebaseStorage.reference(forURL: stream.artwork)
        
        artworkRef.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            
            if let error = error {
                print(error)
            }
            else {
                audioItem.artwork = UIImage(data: data!)
                self.audioPlayer.loadNowPlayingMetaValues()
            }
        }
    }
    
}

//MARK: TableView Delegate & DataSource
extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.radioStreams.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(200.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // get radio stream object
        let radioStream = self.radioStreams[indexPath.row]
        
        // get reuseable custom cell
        let cell: HomeListCell = tableView.dequeueReusableCell(withIdentifier: "HomeListCell") as! HomeListCell
        
        // set logo image on cell
        cell.imgLogo.sd_setImage(with: firebaseStorage.reference(forURL: radioStream.logo_path))
        
        // set background color on cell
        cell.bgView.backgroundColor = UIColor.init(hex: radioStream.background_color)
        
        // assign cell indexPath for later user
        cell.indexPath = indexPath
        
        // assign click actions delegate
        cell.homeListCellDelegate = self
        
        if self.selectedIndexPath != nil {
            if self.selectedIndexPath?.row != indexPath.row {
                cell.showPlayButton()
            }
        }
        
        return cell
    }
    
}

//MARK:- Play Button Action
extension HomeViewController : HomeListCellDelegate {
    
    func playAction(sender: HomeListCell, indexPath: IndexPath) {
        
        self.selectedIndexPath = indexPath
        
        self.playStream(stream: self.radioStreams[indexPath.row])
        
        self.tblView.reloadData()
    }
    
    func pauseAction(sender: HomeListCell, indexPath: IndexPath) {
        
        self.audioPlayer.stop()
        self.tblView.reloadData()
    }
    
}

// MARK: Firebase Stream Data
extension HomeViewController {
    
    func getRadioStreams() {
        
        let db = Firestore.firestore()
        
        db.collection("radio_streams").getDocuments { (querySnapshot, error) in
            
            if let error = error {
                print(error)
                //self.showBanner(title: "Error", subTitle: String(error), type: FAILURE)
            }
            else if let snapshotDocuments = querySnapshot?.documents {
                
                for document in snapshotDocuments {
                    do {
                        if let radioStream = try document.data(as: RadioStreams.self) {
                            self.radioStreams.append(radioStream)
                        }
                    }
                    catch let error as NSError {
                        print(error)
                    }
                }
                
                self.tblView.reloadData()
                
            }
        }
        
    }
}

extension HomeViewController {
    
    func isUserLoggedInAndUnblock () {
        
        if let currentUser = Auth.auth().currentUser {
            
            // show loader
            self.showSpinner(view: self.view)
            
            // user is logged in, check for unblock
            let db = Firestore.firestore()
            
            db.collection("users").document(currentUser.uid).getDocument { (document,error) in
                
                do {
                    if let user = try document?.data(as: User.self) {
                        
                        if user.isBlockedFromChatRoom {
                            
                            // user is blocked from using chatroom
                            self.showBanner(title: "Alert", subTitle: "You are blocked from chatroom", type: WARNING)
                        }
                        else {
                            // user is allowed to use chatroom
                            let vc = ChatScreenViewController.instantiateFromStoryboard()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else {
                        self.showBanner(title: "Error", subTitle: "Failed to get user authentication status", type: FAILURE)
                        let vc = LoginViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                catch _ {
                    self.showBanner(title: "Error", subTitle: "Failed to get user authentication status", type: FAILURE)
                    let vc = LoginViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                // hide loader
                self.hideSpinner(view: self.view)
            }
            
        }
        else {
            let vc = LoginViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
