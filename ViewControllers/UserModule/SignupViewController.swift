//
//  SignupViewController.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 11/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift

class SignupViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SignupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignupViewController
    }
    
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var btnSignup:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
    }
    
    func setupView() {
        
        self.title = "Signup"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        
        self.view.applyGradient(firstColor: Main_View_BG_Color , secondColor: Splash_View_BG_Color)
        self.btnSignup.cornerRadius = self.btnSignup.frame.height * 0.45
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signupAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let fullName = self.txtFullName.text!
        let password = self.txtPassword.text!
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , type:WARNING)
            return
        }
        
        if fullName.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter full name" , type:WARNING)
            return
        }
        
        if password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , type:WARNING)
            return
        }
        
        if password.count < 6 {
            self.showBanner(title: "Alert", subTitle: "Please enter six digit password" , type:WARNING)
            return
        }
        
        signUpUser(email: email, password: password, fullName: fullName)
        
    }
    
    @IBAction func loginAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// Firebase authentication sign up user
extension SignupViewController {
    
    func signUpUser(email: String, password: String, fullName: String) {
        
        // show loader
        self.showSpinner(view: self.view)
        
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            
            if let error = error as NSError? {
                
                switch AuthErrorCode(rawValue: error.code) {
                
                case .operationNotAllowed:
                    // Error: The given sign-in provider is disabled for this Firebase project. Enable it in the Firebase console, under the sign-in method tab of the Auth section.
                    self.showBanner(title: "Error", subTitle: "Action is not allowed", type: FAILURE)
                    
                case .emailAlreadyInUse:
                    // Error: The email address is already in use by another account.
                    self.showBanner(title: "Error", subTitle: "Email already taken. Please use another email", type: FAILURE)
                    
                case .invalidEmail:
                    // Error: The email address is badly formatted.
                    self.showBanner(title: "Error", subTitle: "Invalid email", type: FAILURE)
                    
                case .weakPassword:
                    // Error: The password must be 6 characters long or more.
                    self.showBanner(title: "Error", subTitle: "Password is weak. Please use strong password", type: FAILURE)
                    
                default:
                    print("Error: \(error.localizedDescription)")
                }
                
                
            } else {
                
                // get signed up user info
                let newUserInfo = Auth.auth().currentUser
                
                // add additional user data in firestore
                self.putUserAdditionalData(fullName: fullName, isBlockFromChatRoom: false, uid: newUserInfo!.uid)
                
            }
            
        }
    }
    
    func putUserAdditionalData(fullName: String, isBlockFromChatRoom: Bool, uid: String) {
        
        let db = Firestore.firestore()
        
        let user = User(fullName: fullName, isBlockedFromChatRoom: isBlockFromChatRoom)
        
        do {
            try db.collection("users").document(uid).setData(from: user)
            
            if Auth.auth().currentUser != nil {
                // user is authenticated, move to chatroom
                self.showBanner(title: "Success", subTitle: "User registered successfully", type: SUCCESS)
                
                let vc = ChatScreenViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                // user is not authenticated, something went wrong
                self.showBanner(title: "Error", subTitle: "Something went wrong", type: FAILURE)
            }
            
        } catch let error {
            self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
        }
        
        // hide loader
        self.hideSpinner(view: self.view)
    }
    
}
