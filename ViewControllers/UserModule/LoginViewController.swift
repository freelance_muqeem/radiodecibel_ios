//
//  LoginViewController.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 10/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class LoginViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var btnLogin:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
    }
    
    func setupView() {
        
        self.title = "Login"
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        
        self.view.applyGradient(firstColor: Main_View_BG_Color , secondColor: Splash_View_BG_Color)
        self.btnLogin.cornerRadius = self.btnLogin.frame.height * 0.45
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let password = self.txtPassword.text!
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , type:WARNING)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , type:WARNING)
            return
        }
        
        
        self.signInUser(email: email, password: password)
        
    }
    
    @IBAction func signupAction(_ sender : UIButton) {
        let vc = SignupViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// Firebase Authentication sign in user
extension LoginViewController {
    
    func signInUser(email: String, password: String) {
        
        // show loader
        self.showSpinner(view: self.view)
        
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            
            if let error = error as NSError? {
                
                switch AuthErrorCode(rawValue: error.code) {
                
                case .userDisabled:
                    
                    // Error: The user account has been disabled by an administrator.
                    self.showBanner(title: "Error", subTitle: "Account is disable by admin", type: FAILURE)
                    
                case .wrongPassword:
                    
                    // Error: The password is invalid or the user does not have a password.
                    self.showBanner(title: "Error", subTitle: "Email or password is incorrect", type: FAILURE)
                    
                case .invalidEmail:
                    
                    // Error: Indicates the email address is malformed.
                    self.showBanner(title: "Error", subTitle: "Email or password is incorrect", type: FAILURE)
                    
                default:
                    self.showBanner(title: "Error", subTitle: error.localizedDescription, type: FAILURE)
                }
                
                // hide loader
                self.hideSpinner(view: self.view)
                
            } else {
                
                if let currentUser = Auth.auth().currentUser {
                    
                    // user is logged in, check for unblock
                    let db = Firestore.firestore()
                    
                    db.collection("users").document(currentUser.uid).getDocument { (document,error) in
                        
                        do {
                            if let user = try document?.data(as: User.self) {
                                
                                if user.isBlockedFromChatRoom {
                                    
                                    // user is blocked from using chatroom
                                    self.showBanner(title: "Alert", subTitle: "You are blocked from chatroom", type: WARNING)
                                }
                                else {
                                    // user is allowed to use chatroom
                                    let vc = ChatScreenViewController.instantiateFromStoryboard()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            else {
                                self.showBanner(title: "Error", subTitle: "Failed to get user authentication status", type: FAILURE)
                            }
                        }
                        catch _ {
                            self.showBanner(title: "Error", subTitle: "Failed to get user authentication status", type: FAILURE)
                        }
                        
                        // hide loader
                        self.hideSpinner(view: self.view)
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
