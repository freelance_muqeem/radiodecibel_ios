//
//  HomeViewController.swift
//  Radio Decibel
//
//  Created by Abdul Muqeem on 07/10/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class RootViewController: UINavigationController {
    
    class func instantiateFromStoryboard() -> RootViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}

